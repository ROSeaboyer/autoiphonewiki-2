#!/bin/bash

# SPDX-FileCopyrightText: 2022 Nicolás Alvarez <nicolas.alvarez@gmail.com>
#
# SPDX-License-Identifier: MIT

set -e

./makewiki.py
diff --color -urs wiki_pages output | tee output/all.diff
diffstat output/all.diff
