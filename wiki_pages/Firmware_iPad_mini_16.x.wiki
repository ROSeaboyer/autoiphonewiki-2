<!-- This page is auto-generated from AppleDB data: https://github.com/littlebyteorg/appledb -->
<!-- using this template: https://gitlab.com/nicolas17/autoiphonewiki/-/blob/master/templates/ipadmini16.jinja -->
{{nobots}}
== [[iPad mini (5th generation)]] ==
{| class="wikitable"
|-
! Version
! Build
! Keys
! Baseband
! Release Date
! Download URL
! SHA1 Hash
! File Size
! Documentation
|-
| 16.1
| 20B82
| [[Keys:SydneyB 20B82 (iPad11,1)|iPad11,1]]<br/>[[Keys:SydneyB 20B82 (iPad11,2)|iPad11,2]]
| rowspan="5" | 5.01.01
| {{date|2022|10|24}}
| [https://updates.cdn-apple.com/2022FallFCS/fullrestores/012-92863/34E84A2C-317A-4103-96C8-62D07C6E59EA/iPad_Spring_2019_16.1_20B82_Restore.ipsw iPad_Spring_2019_16.1_20B82_Restore.ipsw]
| <code>2ac145558b12284067a1ba7b1b04b31494691284</code>
| 5,892,799,884
| [https://updates.cdn-apple.com/2022FallFCS/documentation/012-87561/D3B93483-7C2B-496B-A5F0-6C8357E25AEC/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#161 Release Notes]<br/>[https://support.apple.com/HT213489 Security Notes]
|-
| 16.1.1
| 20B101
| [[Keys:SydneyB 20B101 (iPad11,1)|iPad11,1]]<br/>[[Keys:SydneyB 20B101 (iPad11,2)|iPad11,2]]
| {{date|2022|11|09}}
| [https://updates.cdn-apple.com/2022FallFCS/fullrestores/012-97426/1F43D991-0591-447D-B7F9-493913C6036A/iPad_Spring_2019_16.1.1_20B101_Restore.ipsw iPad_Spring_2019_16.1.1_20B101_Restore.ipsw]
| <code>23fcfdd4c7c5ea6fd541edebce145be29c80c53f</code>
| 5,891,923,189
| [https://updates.cdn-apple.com/2022FallFCS/documentation/032-01628/ECB0AF09-F048-4300-AEBB-3EF113A2A152/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1611 Release Notes]<br/>[https://support.apple.com/HT213505 Security Notes]
|-
| 16.2
| 20C65
| [[Keys:SydneyC 20C65 (iPad11,1)|iPad11,1]]<br/>[[Keys:SydneyC 20C65 (iPad11,2)|iPad11,2]]
| {{date|2022|12|13}}
| [https://updates.cdn-apple.com/2022FallFCS/fullrestores/032-14019/7D857460-A49F-4175-8C31-C884BE486FEA/iPad_Spring_2019_16.2_20C65_Restore.ipsw iPad_Spring_2019_16.2_20C65_Restore.ipsw]
| <code>2469f458b07da38ec9fb1e42faf59c70c526936b</code>
| 5,966,142,597
| [https://updates.cdn-apple.com/2022WinterFCS/documentation/032-11205/0F20888C-FB9C-44D0-84FF-50405AF8AD38/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#162 Release Notes]<br/>[https://support.apple.com/HT213530 Security Notes]
|-
| 16.3
| 20D47
| [[Keys:SydneyD 20D47 (iPad11,1)|iPad11,1]]<br/>[[Keys:SydneyD 20D47 (iPad11,2)|iPad11,2]]
| {{date|2023|01|23}}
| [https://updates.cdn-apple.com/2023WinterFCS/fullrestores/032-35847/9C1F82F0-350F-4223-8520-72784FF12B3A/iPad_Spring_2019_16.3_20D47_Restore.ipsw iPad_Spring_2019_16.3_20D47_Restore.ipsw]
| <code>4afef236d323a085f31c2ad0dcac1d2c2ecca74d</code>
| 5,941,188,446
| [https://updates.cdn-apple.com/2023WinterFCS/documentation/032-35610/19B8E936-C7C5-4BAD-8EB6-0FDA92110906/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#163 Release Notes]<br/>[https://support.apple.com/HT213606 Security Notes]
|-
| 16.3.1
| 20D67
| [[Keys:SydneyD 20D67 (iPad11,1)|iPad11,1]]<br/>[[Keys:SydneyD 20D67 (iPad11,2)|iPad11,2]]
| {{date|2023|02|13}}
| [https://updates.cdn-apple.com/2023WinterFCS/fullrestores/032-49698/5DBACB3D-0AF7-4535-BD2A-462C0AF7B0F8/iPad_Spring_2019_16.3.1_20D67_Restore.ipsw iPad_Spring_2019_16.3.1_20D67_Restore.ipsw]
| <code>9558007480354678d9aec03a87aa0bbc90446822</code>
| 5,941,610,952
| [https://updates.cdn-apple.com/2023WinterFCS/documentation/032-44522/631E8840-0A15-4AF2-95BB-DFF58DC5DBDE/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1631 Release Notes]<br/>[https://support.apple.com/HT213635 Security Notes]
|-
| 16.4
| 20E246
| [[Keys:SydneyE 20E246 (iPad11,1)|iPad11,1]]<br/>[[Keys:SydneyE 20E246 (iPad11,2)|iPad11,2]]
| rowspan="4" | 5.02.02
| {{date|2023|03|27}}
| [https://updates.cdn-apple.com/2023WinterSeed/fullrestores/032-66063/C442A2B4-162A-4B4D-BE23-6D0E488AFAD9/iPad_Spring_2019_16.4_20E246_Restore.ipsw iPad_Spring_2019_16.4_20E246_Restore.ipsw]
| <code>bc55ccbc7231ceb2f6f887db6f9cade5ce2521d4</code>
| 6,170,090,534
| [https://updates.cdn-apple.com/2023WinterFCS/documentation/032-66504/9BBF6487-5030-4D3D-8E33-343EFC565EA9/iPadLongRTF.ipd iPadLongRTF.ipd]<br/>[https://support.apple.com/HT213408#164 Release Notes]<br/>[https://support.apple.com/HT213676 Security Notes]
|-
| 16.4.1
| 20E252
| [[Keys:SydneyE 20E252 (iPad11,1)|iPad11,1]]<br/>[[Keys:SydneyE 20E252 (iPad11,2)|iPad11,2]]
| {{date|2023|04|07}}
| [https://updates.cdn-apple.com/2023SpringFCS/fullrestores/032-71041/FB39D81A-6A46-4004-80E6-DD70BE7BB8C1/iPad_Spring_2019_16.4.1_20E252_Restore.ipsw iPad_Spring_2019_16.4.1_20E252_Restore.ipsw]
| <code>9c5d89277ee42b81d42521397aa5ae40d393d139</code>
| 6,170,099,802
| [https://updates.cdn-apple.com/2023FallFCS/documentation/032-71843/93857F06-5B4D-477F-8C96-E42884FA9BA2/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1641 Release Notes]<br/>[https://support.apple.com/HT213720 Security Notes]
|-
| 16.5
| 20F66
| [[Keys:SydneyF 20F66 (iPad11,1)|iPad11,1]]<br/>[[Keys:SydneyF 20F66 (iPad11,2)|iPad11,2]]
| {{date|2023|05|18}}
| [https://updates.cdn-apple.com/2023SpringFCS/fullrestores/032-85323/1C25C1EC-DE75-42D0-BC1E-E8E105336E96/iPad_Spring_2019_16.5_20F66_Restore.ipsw iPad_Spring_2019_16.5_20F66_Restore.ipsw]
| <code>c7ec77ffc9734e7d785372fb3f9c68835125e8b4</code>
| 6,184,456,920
| [https://updates.cdn-apple.com/2022SpringFCS/documentation/032-82421/764434EB-0DD4-4450-95D2-EE204BDD6FC0/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#165 Release Notes]<br/>[https://support.apple.com/HT213757 Security Notes]
|-
| 16.5.1
| 20F75
| [[Keys:SydneyF 20F75 (iPad11,1)|iPad11,1]]<br/>[[Keys:SydneyF 20F75 (iPad11,2)|iPad11,2]]
| {{date|2023|06|21}}
| [https://updates.cdn-apple.com/2023SpringFCS/fullrestores/042-02784/AB148D94-9393-4D9D-A1B4-AB1D30342F47/iPad_Spring_2019_16.5.1_20F75_Restore.ipsw iPad_Spring_2019_16.5.1_20F75_Restore.ipsw]
| <code>87085934d894f13e56f68ee7fe003cd49e1c9df4</code>
| 6,184,343,471
| [https://updates.cdn-apple.com/2023SpringFCS/documentation/032-98218/E5421AB8-87FE-4F61-9DA5-0DE3D2C8F070/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1651 Release Notes]<br/>[https://support.apple.com/HT213814 Security Notes]
|-
| 16.6
| 20G75
| [[Keys:SydneyG 20G75 (iPad11,1)|iPad11,1]]<br/>[[Keys:SydneyG 20G75 (iPad11,2)|iPad11,2]]
| rowspan="5" | 5.03.01
| {{date|2023|07|24}}
| [https://updates.cdn-apple.com/2023SummerFCS/fullrestores/042-17805/A881EEF0-164A-46DC-8E1C-3BD544F49C06/iPad_Spring_2019_16.6_20G75_Restore.ipsw iPad_Spring_2019_16.6_20G75_Restore.ipsw]
| <code>bfe6364ae0e1b80685813266f1364720da6842ca</code>
| 6,186,669,429
| [https://updates.cdn-apple.com/2023SummerFCS/documentation/042-12553/4310BC98-5959-469F-B85F-8536F5F50617/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#166 Release Notes]<br/>[https://support.apple.com/HT213841 Security Notes]
|-
| 16.6.1
| 20G81
| [[Keys:SydneyG 20G81 (iPad11,1)|iPad11,1]]<br/>[[Keys:SydneyG 20G81 (iPad11,2)|iPad11,2]]
| {{date|2023|09|07}}
| [https://updates.cdn-apple.com/2023SummerFCS/fullrestores/042-44566/EAC342DA-468C-46C9-A595-180304491598/iPad_Spring_2019_16.6.1_20G81_Restore.ipsw iPad_Spring_2019_16.6.1_20G81_Restore.ipsw]
| <code>1cf31221ac5e7cf727bbb6234f4281c4fcbfbf79</code>
| 6,187,261,370
| [https://updates.cdn-apple.com/2023SummerFCS/documentation/042-43505/F095284C-B069-416B-BBB7-2B44AD2B9212/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213407#1661 Release Notes]<br/>[https://support.apple.com/HT213905 Security Notes]
|-
| 16.7
| 20H19
| [[Keys:SydneySecurityDawn 20H19 (iPad11,1)|iPad11,1]]<br/>[[Keys:SydneySecurityDawn 20H19 (iPad11,2)|iPad11,2]]
| {{date|2023|09|21}}
| colspan="3" {{n/a}}
| [https://updates.cdn-apple.com/2023FallFCS/documentation/042-43538/E9BC2E32-0A40-40B8-92BC-95AB9C4F15F3/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#167 Release Notes]<br/>[https://support.apple.com/HT213927 Security Notes]
|-
| 16.7.1
| 20H30
| [[Keys:SydneySecurityDawn 20H30 (iPad11,1)|iPad11,1]]<br/>[[Keys:SydneySecurityDawn 20H30 (iPad11,2)|iPad11,2]]
| {{date|2023|10|10}}
| colspan="3" {{n/a}}
| [https://updates.cdn-apple.com/2023FallFCS/documentation/042-41314/7232F047-A81B-496C-9415-F2F83A141646/iPadSydneyUpdateShortRTF.ipd iPadSydneyUpdateShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1671 Release Notes]<br/>[https://support.apple.com/HT213972 Security Notes]
|-
| 16.7.2
| 20H115
| [[Keys:SydneySecurityDawnB 20H115 (iPad11,1)|iPad11,1]]<br/>[[Keys:SydneySecurityDawnB 20H115 (iPad11,2)|iPad11,2]]
| {{date|2023|10|25}}
| colspan="3" {{n/a}}
| [https://updates.cdn-apple.com/2023FallFCS/documentation/042-82927/4E7A8DC8-D6A1-4247-93C4-7CBBC05B303C/iPadSydneyUpdateShortRTF.ipd iPadSydneyUpdateShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1672 Release Notes]<br/>[https://support.apple.com/HT213981 Security Notes]
|}

== [[iPad mini (6th generation)]] ==
{| class="wikitable"
|-
! Version
! Build
! Keys
! Baseband
! Release Date
! Download URL
! SHA1 Hash
! File Size
! Documentation
|-
| 16.1
| 20B82
| [[Keys:SydneyB 20B82 (iPad14,1)|iPad14,1]]<br/>[[Keys:SydneyB 20B82 (iPad14,2)|iPad14,2]]
| rowspan="2" | 2.12.02
| {{date|2022|10|24}}
| [https://updates.cdn-apple.com/2022FallFCS/fullrestores/012-93297/1F544BF5-C17B-4A01-9CBF-D4BABCE0E04B/iPad_Fall_2021_16.1_20B82_Restore.ipsw iPad_Fall_2021_16.1_20B82_Restore.ipsw]
| <code>58537a0f5a71c68b9a8add170b21ef4e7ba1ebdd</code>
| 5,875,352,581
| [https://updates.cdn-apple.com/2022FallFCS/documentation/012-87561/D3B93483-7C2B-496B-A5F0-6C8357E25AEC/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#161 Release Notes]<br/>[https://support.apple.com/HT213489 Security Notes]
|-
| 16.1.1
| 20B101
| [[Keys:SydneyB 20B101 (iPad14,1)|iPad14,1]]<br/>[[Keys:SydneyB 20B101 (iPad14,2)|iPad14,2]]
| {{date|2022|11|09}}
| [https://updates.cdn-apple.com/2022FallFCS/fullrestores/012-97326/7EC02507-9A66-4D24-B01E-33207F68AFF0/iPad_Fall_2021_16.1.1_20B101_Restore.ipsw iPad_Fall_2021_16.1.1_20B101_Restore.ipsw]
| <code>3c0d434f8fe7b3d2723b81f522afebd4ddc2ce97</code>
| 5,875,124,360
| [https://updates.cdn-apple.com/2022FallFCS/documentation/032-01628/ECB0AF09-F048-4300-AEBB-3EF113A2A152/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1611 Release Notes]<br/>[https://support.apple.com/HT213505 Security Notes]
|-
| 16.2
| 20C65
| [[Keys:SydneyC 20C65 (iPad14,1)|iPad14,1]]<br/>[[Keys:SydneyC 20C65 (iPad14,2)|iPad14,2]]
| 2.21.00
| {{date|2022|12|13}}
| [https://updates.cdn-apple.com/2022FallFCS/fullrestores/032-13980/50636F1D-649B-4E1E-B15F-E76CC9886C25/iPad_Fall_2021_16.2_20C65_Restore.ipsw iPad_Fall_2021_16.2_20C65_Restore.ipsw]
| <code>e55bf2288c57f1555c75965f2e20025c0b09a159</code>
| 5,948,846,616
| [https://updates.cdn-apple.com/2022WinterFCS/documentation/032-11205/0F20888C-FB9C-44D0-84FF-50405AF8AD38/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#162 Release Notes]<br/>[https://support.apple.com/HT213530 Security Notes]
|-
| 16.3
| 20D47
| [[Keys:SydneyD 20D47 (iPad14,1)|iPad14,1]]<br/>[[Keys:SydneyD 20D47 (iPad14,2)|iPad14,2]]
| rowspan="2" | 2.40.01
| {{date|2023|01|23}}
| [https://updates.cdn-apple.com/2023WinterFCS/fullrestores/032-35943/4047B5A3-018E-4CC0-9257-03AD5F01FBA8/iPad_Fall_2021_16.3_20D47_Restore.ipsw iPad_Fall_2021_16.3_20D47_Restore.ipsw]
| <code>307da2103b7ec35b94f8cfffcd60291ca4c4421e</code>
| 5,955,787,501
| [https://updates.cdn-apple.com/2023WinterFCS/documentation/032-35610/19B8E936-C7C5-4BAD-8EB6-0FDA92110906/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#163 Release Notes]<br/>[https://support.apple.com/HT213606 Security Notes]
|-
| 16.3.1
| 20D67
| [[Keys:SydneyD 20D67 (iPad14,1)|iPad14,1]]<br/>[[Keys:SydneyD 20D67 (iPad14,2)|iPad14,2]]
| {{date|2023|02|13}}
| [https://updates.cdn-apple.com/2023WinterFCS/fullrestores/032-50137/D4A26580-0EBB-40A6-9C59-037D9B800D80/iPad_Fall_2021_16.3.1_20D67_Restore.ipsw iPad_Fall_2021_16.3.1_20D67_Restore.ipsw]
| <code>8d0ec1b610537de92490350c4cc2aa38a335930e</code>
| 5,955,979,705
| [https://updates.cdn-apple.com/2023WinterFCS/documentation/032-44522/631E8840-0A15-4AF2-95BB-DFF58DC5DBDE/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1631 Release Notes]<br/>[https://support.apple.com/HT213635 Security Notes]
|-
| 16.4
| 20E246
| [[Keys:SydneyE 20E246 (iPad14,1)|iPad14,1]]<br/>[[Keys:SydneyE 20E246 (iPad14,2)|iPad14,2]]
| rowspan="2" | 2.55.00
| {{date|2023|03|27}}
| [https://updates.cdn-apple.com/2023WinterSeed/fullrestores/032-66084/1144F84D-E41B-435E-A249-73965D4CEEBE/iPad_Fall_2021_16.4_20E246_Restore.ipsw iPad_Fall_2021_16.4_20E246_Restore.ipsw]
| <code>407686583ac941037e1a445002895cdb9c4be85e</code>
| 6,189,862,929
| [https://updates.cdn-apple.com/2023WinterFCS/documentation/032-66504/9BBF6487-5030-4D3D-8E33-343EFC565EA9/iPadLongRTF.ipd iPadLongRTF.ipd]<br/>[https://support.apple.com/HT213408#164 Release Notes]<br/>[https://support.apple.com/HT213676 Security Notes]
|-
| 16.4.1
| 20E252
| [[Keys:SydneyE 20E252 (iPad14,1)|iPad14,1]]<br/>[[Keys:SydneyE 20E252 (iPad14,2)|iPad14,2]]
| {{date|2023|04|07}}
| [https://updates.cdn-apple.com/2023SpringFCS/fullrestores/032-71293/B291ABC2-FA99-44D4-83CF-DE5F7497C22A/iPad_Fall_2021_16.4.1_20E252_Restore.ipsw iPad_Fall_2021_16.4.1_20E252_Restore.ipsw]
| <code>1bca529253bcd3ea580db62a6b8ce3f67f09c6e1</code>
| 6,189,751,662
| [https://updates.cdn-apple.com/2023FallFCS/documentation/032-71843/93857F06-5B4D-477F-8C96-E42884FA9BA2/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1641 Release Notes]<br/>[https://support.apple.com/HT213720 Security Notes]
|-
| 16.5
| 20F66
| [[Keys:SydneyF 20F66 (iPad14,1)|iPad14,1]]<br/>[[Keys:SydneyF 20F66 (iPad14,2)|iPad14,2]]
| rowspan="2" | 2.70.01
| {{date|2023|05|18}}
| [https://updates.cdn-apple.com/2023SpringFCS/fullrestores/032-85162/D4C96AC6-95E2-41B3-80D0-662337E296AD/iPad_Fall_2021_16.5_20F66_Restore.ipsw iPad_Fall_2021_16.5_20F66_Restore.ipsw]
| <code>e4d3adac327169de0566e978142adac783e09ac3</code>
| 6,204,532,790
| [https://updates.cdn-apple.com/2022SpringFCS/documentation/032-82421/764434EB-0DD4-4450-95D2-EE204BDD6FC0/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#165 Release Notes]<br/>[https://support.apple.com/HT213757 Security Notes]
|-
| 16.5.1
| 20F75
| [[Keys:SydneyF 20F75 (iPad14,1)|iPad14,1]]<br/>[[Keys:SydneyF 20F75 (iPad14,2)|iPad14,2]]
| {{date|2023|06|21}}
| [https://updates.cdn-apple.com/2023SpringFCS/fullrestores/042-02549/D138E3C8-2CF9-4204-9D9E-D73DE9227AD6/iPad_Fall_2021_16.5.1_20F75_Restore.ipsw iPad_Fall_2021_16.5.1_20F75_Restore.ipsw]
| <code>609d06efd728d2169289d741b3b941c91f7f301e</code>
| 6,204,516,112
| [https://updates.cdn-apple.com/2023SpringFCS/documentation/032-98218/E5421AB8-87FE-4F61-9DA5-0DE3D2C8F070/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1651 Release Notes]<br/>[https://support.apple.com/HT213814 Security Notes]
|-
| 16.6
| 20G75
| [[Keys:SydneyG 20G75 (iPad14,1)|iPad14,1]]<br/>[[Keys:SydneyG 20G75 (iPad14,2)|iPad14,2]]
| rowspan="5" | 2.80.01
| {{date|2023|07|24}}
| [https://updates.cdn-apple.com/2023SummerFCS/fullrestores/042-18000/581078D6-3F47-4F26-8024-A14A437240CA/iPad_Fall_2021_16.6_20G75_Restore.ipsw iPad_Fall_2021_16.6_20G75_Restore.ipsw]
| <code>19888a83e3d45c16bca84ed4c760a2756ee6b0db</code>
| 6,205,997,472
| [https://updates.cdn-apple.com/2023SummerFCS/documentation/042-12553/4310BC98-5959-469F-B85F-8536F5F50617/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#166 Release Notes]<br/>[https://support.apple.com/HT213841 Security Notes]
|-
| 16.6.1
| 20G81
| [[Keys:SydneyG 20G81 (iPad14,1)|iPad14,1]]<br/>[[Keys:SydneyG 20G81 (iPad14,2)|iPad14,2]]
| {{date|2023|09|07}}
| [https://updates.cdn-apple.com/2023SummerFCS/fullrestores/042-44410/90DC2BF6-99CF-4393-89F6-B97332806D9B/iPad_Fall_2021_16.6.1_20G81_Restore.ipsw iPad_Fall_2021_16.6.1_20G81_Restore.ipsw]
| <code>ef06d988c40bf9ccfb6cad498fcf3d03f7743314</code>
| 6,205,946,239
| [https://updates.cdn-apple.com/2023SummerFCS/documentation/042-43505/F095284C-B069-416B-BBB7-2B44AD2B9212/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213407#1661 Release Notes]<br/>[https://support.apple.com/HT213905 Security Notes]
|-
| 16.7
| 20H19
| [[Keys:SydneySecurityDawn 20H19 (iPad14,1)|iPad14,1]]<br/>[[Keys:SydneySecurityDawn 20H19 (iPad14,2)|iPad14,2]]
| {{date|2023|09|21}}
| colspan="3" {{n/a}}
| [https://updates.cdn-apple.com/2023FallFCS/documentation/042-43538/E9BC2E32-0A40-40B8-92BC-95AB9C4F15F3/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#167 Release Notes]<br/>[https://support.apple.com/HT213927 Security Notes]
|-
| 16.7.1
| 20H30
| [[Keys:SydneySecurityDawn 20H30 (iPad14,1)|iPad14,1]]<br/>[[Keys:SydneySecurityDawn 20H30 (iPad14,2)|iPad14,2]]
| {{date|2023|10|10}}
| colspan="3" {{n/a}}
| [https://updates.cdn-apple.com/2023FallFCS/documentation/042-41314/7232F047-A81B-496C-9415-F2F83A141646/iPadSydneyUpdateShortRTF.ipd iPadSydneyUpdateShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1671 Release Notes]<br/>[https://support.apple.com/HT213972 Security Notes]
|-
| 16.7.2
| 20H115
| [[Keys:SydneySecurityDawnB 20H115 (iPad14,1)|iPad14,1]]<br/>[[Keys:SydneySecurityDawnB 20H115 (iPad14,2)|iPad14,2]]
| {{date|2023|10|25}}
| colspan="3" {{n/a}}
| [https://updates.cdn-apple.com/2023FallFCS/documentation/042-82927/4E7A8DC8-D6A1-4247-93C4-7CBBC05B303C/iPadSydneyUpdateShortRTF.ipd iPadSydneyUpdateShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1672 Release Notes]<br/>[https://support.apple.com/HT213981 Security Notes]
|}

[[Category:Firmware]]