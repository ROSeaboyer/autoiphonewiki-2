<!-- This page is auto-generated from AppleDB data: https://github.com/littlebyteorg/appledb -->
<!-- using this template: https://gitlab.com/nicolas17/autoiphonewiki/-/blob/master/templates/homepod15.jinja -->
{{nobots}}
== [[HomePod]] ==
{| class="wikitable"
|-
! Version
! Build
! Keys
! Release Date
! Documentation
|-
| 15.0
| 19J346
| [[Keys:Satellite 19J346 (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:Satellite 19J346 (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2021|09|20}}
| [https://support.apple.com/HT208714#15 Release Notes]
|-
| 15.1
| 19J572
| [[Keys:SatelliteB 19J572 (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteB 19J572 (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2021|10|25}}
| [https://support.apple.com/HT208714#151 Release Notes]
|-
| 15.1.1
| 19J582
| [[Keys:SatelliteB 19J582 (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteB 19J582 (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2021|11|03}}
| [https://support.apple.com/HT208714#1511 Release Notes]
|-
| 15.2
| 19K52
| [[Keys:SatelliteC 19K52 (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteC 19K52 (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2021|12|13}}
| [https://support.apple.com/HT208714#152 Release Notes]
|-
| 15.3
| 19K547
| [[Keys:SatelliteD 19K547 (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteD 19K547 (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2022|01|26}}
| [https://support.apple.com/HT208714#153 Release Notes]
|-
| 15.4
| 19L440
| [[Keys:SatelliteE 19L440 (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteE 19L440 (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2022|03|14}}
| [https://support.apple.com/HT208714#154 Release Notes]
|-
| 15.4.1
| 19L452
| [[Keys:SatelliteE 19L452 (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteE 19L452 (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2022|03|31}}
| [https://support.apple.com/HT208714#1541 Release Notes]
|-
| 15.5
| 19L570
| [[Keys:SatelliteF 19L570 (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteF 19L570 (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2022|05|16}}
| [https://support.apple.com/HT208714#155 Release Notes]
|-
| 15.5.1
| 19L580
| [[Keys:SatelliteF 19L580 (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteF 19L580 (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2022|05|25}}
| [https://support.apple.com/HT208714#1551 Release Notes]
|-
| 15.6
| 19M65
| [[Keys:SatelliteG 19M65 (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteG 19M65 (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2022|07|20}}
| [https://support.apple.com/HT208714#156 Release Notes]
|}

== [[HomePod mini]] ==
{| class="wikitable"
|-
! Version
! Build
! Keys
! Release Date
! Download URL
! SHA1 Hash
! File Size
! Documentation
|-
| 15.0
| 19J346
| [[Keys:Satellite 19J346 (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2021|09|20}}
| [https://updates.cdn-apple.com/2021FallFCS/fullrestores/071-91546/3B39CCC4-235E-4CCE-BC0D-2AF558F04802/AudioAccessory5,1_15.0_19J346_Restore.ipsw AudioAccessory5,1_15.0_19J346_Restore.ipsw]
| <code>e448b8095d31e1f43628b1b0825402a40e65e70b</code>
| 2,584,189,711
| [https://updates.cdn-apple.com/2021SummerFCS/documentation/071-71741/014FC51A-15D2-41E2-AA20-E3FA2C667893/HomePodiTunesUpdateReadMe.ipd HomePodiTunesUpdateReadMe.ipd]<br/>[https://support.apple.com/HT208714#15 Release Notes]
|-
| 15.1
| 19J572
| [[Keys:SatelliteB 19J572 (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2021|10|25}}
| [https://updates.cdn-apple.com/2021FCSFall/fullrestores/002-21878/7A6E1414-7FD5-4EF6-A7AD-897E23A94274/AudioAccessory5,1_15.1_19J572_Restore.ipsw AudioAccessory5,1_15.1_19J572_Restore.ipsw]
| <code>751ae5c8d920f3e6323cb278c1df78885e889da3</code>
| 2,580,305,269
| [https://updates.cdn-apple.com/2021FCSFall/documentation/002-18964/8158A7E9-1682-4228-9103-6F4DEE9C7EE2/HomePodiTunesUpdateReadMe.ipd HomePodiTunesUpdateReadMe.ipd]<br/>[https://support.apple.com/HT208714#151 Release Notes]
|-
| 15.1.1
| 19J582
| [[Keys:SatelliteB 19J582 (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2021|11|03}}
| [https://updates.cdn-apple.com/2021FCSFall/fullrestores/002-30167/FDF89B7E-DF25-4CA0-AEF9-DB04109390D1/AudioAccessory5,1_15.1.1_19J582_Restore.ipsw AudioAccessory5,1_15.1.1_19J582_Restore.ipsw]
| <code>c4cb71f0e2e3caa3521d9de6b4d845cd5a3719d4</code>
| 2,581,528,891
| [https://updates.cdn-apple.com/2021FCSFall/documentation/002-30528/B072D362-5975-48D0-9441-28A5C7C36B0F/HomePodiTunesUpdateReadMe.ipd HomePodiTunesUpdateReadMe.ipd]<br/>[https://support.apple.com/HT208714#1511 Release Notes]
|-
| 15.2
| 19K52
| [[Keys:SatelliteC 19K52 (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2021|12|13}}
| [https://updates.cdn-apple.com/2021FCSWinter/fullrestores/071-93727/F505D53E-EFC6-42B2-BAC6-C3CFBB6BDDAC/AudioAccessory5,1_15.2_19K52_Restore.ipsw AudioAccessory5,1_15.2_19K52_Restore.ipsw]
| <code>f826a137b780df4bbb086d2f723cb46b8064d8a1</code>
| 2,624,235,936
| [https://updates.cdn-apple.com/2021WinterFCS/documentation/002-43821/A9725D59-A7C2-42E4-A909-B333E695FCD7/HomePodRTF.ipd HomePodRTF.ipd]<br/>[https://support.apple.com/HT208714#152 Release Notes]
|-
| 15.3
| 19K547
| [[Keys:SatelliteD 19K547 (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2022|01|26}}
| [https://updates.cdn-apple.com/2022FCSWinter/fullrestores/002-56675/8589E44F-C93F-410C-B81C-0C2ECF04210E/AudioAccessory5,1_15.3_19K547_Restore.ipsw AudioAccessory5,1_15.3_19K547_Restore.ipsw]
| <code>351badd53a93eafcfad65a6fcbcf035b3dcd83ab</code>
| 2,631,449,292
| [https://updates.cdn-apple.com/2021WinterFCS/documentation/002-43821/A9725D59-A7C2-42E4-A909-B333E695FCD7/HomePodRTF.ipd HomePodRTF.ipd]<br/>[https://support.apple.com/HT208714#153 Release Notes]
|-
| 15.4
| 19L440
| [[Keys:SatelliteE 19L440 (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2022|03|14}}
| [https://updates.cdn-apple.com/2022FCSWinter/fullrestores/071-05891/E1B66580-478B-4517-A128-ED6B87956E60/AudioAccessory5,1_15.4_19L440_Restore.ipsw AudioAccessory5,1_15.4_19L440_Restore.ipsw]
| <code>f9671e539a3caa3e74d621b004fe2da8e7aae424</code>
| 2,661,467,421
| [https://updates.cdn-apple.com/2022FCSWinter/documentation/002-76882/0D51A92E-F116-40D6-917A-F77BA50AEA75/HomePodRTF.ipd HomePodRTF.ipd]<br/>[https://support.apple.com/HT208714#154 Release Notes]
|-
| 15.4.1
| 19L452
| [[Keys:SatelliteE 19L452 (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2022|03|31}}
| [https://updates.cdn-apple.com/2022FCSWinter/fullrestores/002-82880/68A76681-76AD-483B-9F10-ED2AFA21013A/AudioAccessory5,1_15.4.1_19L452_Restore.ipsw AudioAccessory5,1_15.4.1_19L452_Restore.ipsw]
| <code>596c8c83a9832b79922b90ae79cd17a1e11ffd9d</code>
| 2,661,241,838
| [https://updates.cdn-apple.com/2022SpringFCS/documentation/002-84227/82C6FA71-8820-4FC7-8348-E23FEAC91227/HomePodRTF.ipd HomePodRTF.ipd]<br/>[https://support.apple.com/HT208714#1541 Release Notes]
|-
| 15.5
| 19L570
| [[Keys:SatelliteF 19L570 (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2022|05|16}}
| [https://updates.cdn-apple.com/2022FCSSpring/fullrestores/002-53892/4705F59B-47F5-430B-8D2F-1EF9D36AB2CE/AudioAccessory5,1_15.5_19L570_Restore.ipsw AudioAccessory5,1_15.5_19L570_Restore.ipsw]
| <code>b87c7e747f78b6ccc6577350fc5e6075b568062a</code>
| 2,665,447,144
| [https://updates.cdn-apple.com/2022SpringFCS/documentation/012-04542/E7D8E82C-77E0-48C7-AD06-F222C2111DFE/HomePodRTF.ipd HomePodRTF.ipd]<br/>[https://support.apple.com/HT208714#155 Release Notes]
|-
| 15.5.1
| 19L580
| [[Keys:SatelliteF 19L580 (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2022|05|25}}
| [https://updates.cdn-apple.com/2022FCSSpring/fullrestores/012-11878/F6CC6C19-5DC2-4AF5-B58C-39ECEFF4128A/AudioAccessory5,1_15.5.1_19L580_Restore.ipsw AudioAccessory5,1_15.5.1_19L580_Restore.ipsw]
| <code>26e359963ce2ab8b94fae0eef6890d0ce26c5de9</code>
| 2,665,843,718
| [https://updates.cdn-apple.com/2022SpringFCS/documentation/012-13049/EC1DDC1D-E3DA-48A4-8CF5-1023D71C2EFD/HomePodRTF.ipd HomePodRTF.ipd]<br/>[https://support.apple.com/HT208714#1551 Release Notes]
|-
| 15.6
| 19M65
| [[Keys:SatelliteG 19M65 (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2022|07|20}}
| [https://updates.cdn-apple.com/2022FCSSummer/fullrestores/012-41803/9750343B-BCA9-4FF7-BD01-B2E75F1D9734/AudioAccessory5,1_15.6_19M65_Restore.ipsw AudioAccessory5,1_15.6_19M65_Restore.ipsw]
| <code>687174e0caf437099c695a4d9fdab4c2bb75c483</code>
| 2,709,124,263
| [https://updates.cdn-apple.com/2022SummerFCS/documentation/012-39113/E22A0925-088A-4291-B4B9-237B87A4F02F/HomePodRTF.ipd HomePodRTF.ipd]<br/>[https://support.apple.com/HT208714#156 Release Notes]
|}

[[Category:Firmware]]