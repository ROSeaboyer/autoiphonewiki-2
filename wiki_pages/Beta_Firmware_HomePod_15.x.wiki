<!-- This page is auto-generated from AppleDB data: https://github.com/littlebyteorg/appledb -->
<!-- using this template: https://gitlab.com/nicolas17/autoiphonewiki/-/blob/master/templates/homepod15-beta.jinja -->
{{nobots}}
== [[HomePod]] ==
{| class="wikitable"
|-
! Version
! Build
! Keys
! Release Date
|-
| 15.0 beta
| 19J5268r
| [[Keys:SatelliteSeed 19J5268r (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteSeed 19J5268r (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2021|06|07}}
|-
| 15.0 beta 2
| 19J5288e
| [[Keys:SatelliteSeed 19J5288e (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteSeed 19J5288e (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2021|06|24}}
|-
| rowspan="2" | 15.0 beta 3
| 19J5304b
| [[Keys:SatelliteSeed 19J5304b (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteSeed 19J5304b (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2021|07|12}}
|-
| 19J5304e
| [[Keys:SatelliteSeed 19J5304e (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteSeed 19J5304e (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2021|07|14}}
|-
| 15.0 beta 4
| 19J5314e
| [[Keys:SatelliteSeed 19J5314e (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteSeed 19J5314e (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2021|07|27}}
|-
| 15.0 beta 5
| 19J5332b
| [[Keys:SatelliteSeed 19J5332b (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteSeed 19J5332b (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2021|08|12}}
|-
| 15.0 beta 6
| 19J5332d
| [[Keys:SatelliteSeed 19J5332d (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteSeed 19J5332d (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2021|08|17}}
|-
| 15.0 beta 7
| 19J5340a
| [[Keys:SatelliteSeed 19J5340a (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteSeed 19J5340a (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2021|08|25}}
|-
| 15.0 beta 8
| 19J5345a
| [[Keys:SatelliteSeed 19J5345a (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteSeed 19J5345a (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2021|08|31}}
|-
| 15.0 beta 9
| 19J5346a
| [[Keys:SatelliteSeed 19J5346a (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteSeed 19J5346a (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2021|09|07}}
|-
| 15.0 [[Release Candidate|RC]]
| 19J346
| [[Keys:Satellite 19J346 (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:Satellite 19J346 (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2021|09|14}}
|-
| 15.1 beta
| 19J5542e
| [[Keys:SatelliteBSeed 19J5542e (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteBSeed 19J5542e (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2021|09|21}}
|-
| 15.1 beta 2
| 19J5552e
| [[Keys:SatelliteBSeed 19J5552e (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteBSeed 19J5552e (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2021|09|28}}
|-
| 15.1 beta 3
| 19J5560d
| [[Keys:SatelliteBSeed 19J5560d (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteBSeed 19J5560d (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2021|10|06}}
|-
| 15.1 beta 4
| 19J5567a
| [[Keys:SatelliteBSeed 19J5567a (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteBSeed 19J5567a (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2021|10|13}}
|-
| 15.1 [[Release Candidate|RC]]
| 19J572
| [[Keys:SatelliteB 19J572 (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteB 19J572 (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2021|10|18}}
|-
| 15.2 beta
| 19K5025g
| [[Keys:SatelliteCSeed 19K5025g (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteCSeed 19K5025g (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2021|10|27}}
|-
| 15.2 beta 2
| 19K5035d
| [[Keys:SatelliteCSeed 19K5035d (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteCSeed 19K5035d (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2021|11|09}}
|-
| 15.2 beta 3
| 19K5043b
| [[Keys:SatelliteCSeed 19K5043b (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteCSeed 19K5043b (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2021|11|16}}
|-
| 15.2 beta 4
| 19K5050a
| [[Keys:SatelliteCSeed 19K5050a (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteCSeed 19K5050a (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2021|12|02}}
|-
| 15.2 [[Release Candidate|RC]]
| 19K52
| [[Keys:SatelliteC 19K52 (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteC 19K52 (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2021|12|08}}
|-
| 15.3 beta
| 19K5527d
| [[Keys:SatelliteDSeed 19K5527d (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteDSeed 19K5527d (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2021|12|17}}
|-
| 15.3 beta 2
| 19K5541d
| [[Keys:SatelliteDSeed 19K5541d (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteDSeed 19K5541d (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2022|01|12}}
|-
| 15.3 [[Release Candidate|RC]]
| 19K546
| [[Keys:SatelliteD 19K546 (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteD 19K546 (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2022|01|20}}
|-
| 15.4 beta
| 19L5409j
| [[Keys:SatelliteESeed 19L5409j (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteESeed 19L5409j (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2022|01|27}}
|-
| 15.4 beta 2
| 19L5419e
| [[Keys:SatelliteESeed 19L5419e (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteESeed 19L5419e (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2022|02|08}}
|-
| 15.4 beta 3
| 19L5425e
| [[Keys:SatelliteESeed 19L5425e (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteESeed 19L5425e (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2022|02|15}}
|-
| 15.4 beta 4
| 19L5436a
| [[Keys:SatelliteESeed 19L5436a (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteESeed 19L5436a (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2022|02|22}}
|-
| 15.4 beta 5
| 19L5440a
| [[Keys:SatelliteESeed 19L5440a (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteESeed 19L5440a (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2022|03|01}}
|-
| 15.4 [[Release Candidate|RC]]
| 19L440
| [[Keys:SatelliteE 19L440 (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteE 19L440 (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2022|03|08}}
|-
| 15.5 beta
| 19L5547e
| [[Keys:SatelliteFSeed 19L5547e (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteFSeed 19L5547e (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2022|04|05}}
|-
| 15.5 beta 2
| 19L5557d
| [[Keys:SatelliteFSeed 19L5557d (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteFSeed 19L5557d (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2022|04|19}}
|-
| 15.5 beta 3
| 19L5562e
| [[Keys:SatelliteFSeed 19L5562e (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteFSeed 19L5562e (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2022|04|26}}
|-
| 15.5 beta 4
| 19L5569a
| [[Keys:SatelliteFSeed 19L5569a (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteFSeed 19L5569a (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2022|05|03}}
|-
| 15.5 [[Release Candidate|RC]]
| 19L570
| [[Keys:SatelliteF 19L570 (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteF 19L570 (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2022|05|12}}
|-
| 15.6 beta
| 19M5027c
| [[Keys:SatelliteGSeed 19M5027c (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteGSeed 19M5027c (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2022|05|18}}
|-
| 15.6 beta 2
| 19M5037c
| [[Keys:SatelliteGSeed 19M5037c (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteGSeed 19M5037c (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2022|05|31}}
|-
| 15.6 beta 3
| 19M5046c
| [[Keys:SatelliteGSeed 19M5046c (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteGSeed 19M5046c (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2022|06|14}}
|-
| 15.6 beta 4
| 19M5056c
| [[Keys:SatelliteGSeed 19M5056c (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteGSeed 19M5056c (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2022|06|28}}
|-
| 15.6 beta 5
| 19M5062a
| [[Keys:SatelliteGSeed 19M5062a (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteGSeed 19M5062a (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2022|07|05}}
|-
| 15.6 [[Release Candidate|RC]]
| 19M63
| [[Keys:SatelliteG 19M63 (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:SatelliteG 19M63 (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2022|07|12}}
|}

== [[HomePod mini]] ==
{| class="wikitable"
|-
! Version
! Build
! Keys
! Release Date
|-
| 15.0 beta
| 19J5268r
| [[Keys:SatelliteSeed 19J5268r (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2021|06|07}}
|-
| 15.0 beta 2
| 19J5288e
| [[Keys:SatelliteSeed 19J5288e (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2021|06|24}}
|-
| rowspan="2" | 15.0 beta 3
| 19J5304b
| [[Keys:SatelliteSeed 19J5304b (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2021|07|12}}
|-
| 19J5304e
| [[Keys:SatelliteSeed 19J5304e (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2021|07|14}}
|-
| 15.0 beta 4
| 19J5314e
| [[Keys:SatelliteSeed 19J5314e (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2021|07|27}}
|-
| 15.0 beta 5
| 19J5332b
| [[Keys:SatelliteSeed 19J5332b (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2021|08|12}}
|-
| 15.0 beta 6
| 19J5332d
| [[Keys:SatelliteSeed 19J5332d (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2021|08|17}}
|-
| 15.0 beta 7
| 19J5340a
| [[Keys:SatelliteSeed 19J5340a (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2021|08|25}}
|-
| 15.0 beta 8
| 19J5345a
| [[Keys:SatelliteSeed 19J5345a (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2021|08|31}}
|-
| 15.0 beta 9
| 19J5346a
| [[Keys:SatelliteSeed 19J5346a (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2021|09|07}}
|-
| 15.0 [[Release Candidate|RC]]
| 19J346
| [[Keys:Satellite 19J346 (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2021|09|14}}
|-
| 15.1 beta
| 19J5542e
| [[Keys:SatelliteBSeed 19J5542e (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2021|09|21}}
|-
| 15.1 beta 2
| 19J5552e
| [[Keys:SatelliteBSeed 19J5552e (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2021|09|28}}
|-
| 15.1 beta 3
| 19J5560d
| [[Keys:SatelliteBSeed 19J5560d (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2021|10|06}}
|-
| 15.1 beta 4
| 19J5567a
| [[Keys:SatelliteBSeed 19J5567a (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2021|10|13}}
|-
| 15.1 [[Release Candidate|RC]]
| 19J572
| [[Keys:SatelliteB 19J572 (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2021|10|18}}
|-
| 15.2 beta
| 19K5025g
| [[Keys:SatelliteCSeed 19K5025g (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2021|10|27}}
|-
| 15.2 beta 2
| 19K5035d
| [[Keys:SatelliteCSeed 19K5035d (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2021|11|09}}
|-
| 15.2 beta 3
| 19K5043b
| [[Keys:SatelliteCSeed 19K5043b (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2021|11|16}}
|-
| 15.2 beta 4
| 19K5050a
| [[Keys:SatelliteCSeed 19K5050a (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2021|12|02}}
|-
| 15.2 [[Release Candidate|RC]]
| 19K52
| [[Keys:SatelliteC 19K52 (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2021|12|08}}
|-
| 15.3 beta
| 19K5527d
| [[Keys:SatelliteDSeed 19K5527d (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2021|12|17}}
|-
| 15.3 beta 2
| 19K5541d
| [[Keys:SatelliteDSeed 19K5541d (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2022|01|12}}
|-
| 15.3 [[Release Candidate|RC]]
| 19K546
| [[Keys:SatelliteD 19K546 (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2022|01|20}}
|-
| 15.4 beta
| 19L5409j
| [[Keys:SatelliteESeed 19L5409j (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2022|01|27}}
|-
| 15.4 beta 2
| 19L5419e
| [[Keys:SatelliteESeed 19L5419e (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2022|02|08}}
|-
| 15.4 beta 3
| 19L5425e
| [[Keys:SatelliteESeed 19L5425e (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2022|02|15}}
|-
| 15.4 beta 4
| 19L5436a
| [[Keys:SatelliteESeed 19L5436a (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2022|02|22}}
|-
| 15.4 beta 5
| 19L5440a
| [[Keys:SatelliteESeed 19L5440a (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2022|03|01}}
|-
| 15.4 [[Release Candidate|RC]]
| 19L440
| [[Keys:SatelliteE 19L440 (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2022|03|08}}
|-
| 15.5 beta
| 19L5547e
| [[Keys:SatelliteFSeed 19L5547e (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2022|04|05}}
|-
| 15.5 beta 2
| 19L5557d
| [[Keys:SatelliteFSeed 19L5557d (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2022|04|19}}
|-
| 15.5 beta 3
| 19L5562e
| [[Keys:SatelliteFSeed 19L5562e (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2022|04|26}}
|-
| 15.5 beta 4
| 19L5569a
| [[Keys:SatelliteFSeed 19L5569a (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2022|05|03}}
|-
| 15.5 [[Release Candidate|RC]]
| 19L570
| [[Keys:SatelliteF 19L570 (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2022|05|12}}
|-
| 15.6 beta
| 19M5027c
| [[Keys:SatelliteGSeed 19M5027c (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2022|05|18}}
|-
| 15.6 beta 2
| 19M5037c
| [[Keys:SatelliteGSeed 19M5037c (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2022|05|31}}
|-
| 15.6 beta 3
| 19M5046c
| [[Keys:SatelliteGSeed 19M5046c (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2022|06|14}}
|-
| 15.6 beta 4
| 19M5056c
| [[Keys:SatelliteGSeed 19M5056c (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2022|06|28}}
|-
| 15.6 beta 5
| 19M5062a
| [[Keys:SatelliteGSeed 19M5062a (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2022|07|05}}
|-
| 15.6 [[Release Candidate|RC]]
| 19M63
| [[Keys:SatelliteG 19M63 (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2022|07|12}}
|}

[[Category:Firmware]]