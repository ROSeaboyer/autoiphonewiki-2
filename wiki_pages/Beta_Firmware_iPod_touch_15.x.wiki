<!-- This page is auto-generated from AppleDB data: https://github.com/littlebyteorg/appledb -->
<!-- using this template: https://gitlab.com/nicolas17/autoiphonewiki/-/blob/master/templates/ipod15-beta.jinja -->
{{nobots}}
== [[iPod touch (7th generation)]] ==
{| class="wikitable"
|-
! Version
! Build
! Keys
! Release Date
! Download URL
! File Size
|-
| 15.0 beta
| 19A5261w
| [[Keys:SkySeed 19A5261w (iPod9,1)|iPod9,1]]
| {{date|2021|06|07}}
| [https://developer.apple.com/services-account/download?path=/WWDC_2021/iOS_15_beta/iPodtouch_7_15.0_19A5261w_Restore.ipsw iPodtouch_7_15.0_19A5261w_Restore.ipsw]
| 5,284,280,366
|-
| rowspan="2" | 15.0 beta 2
| 19A5281h
| [[Keys:SkySeed 19A5281h (iPod9,1)|iPod9,1]]
| {{date|2021|06|24}}
| [https://updates.cdn-apple.com/2021SummerSeed/fullrestores/071-59183/7B5769CF-6EE2-4747-B7B2-9146D3EF4D2B/iPodtouch_7_15.0_19A5281h_Restore.ipsw iPodtouch_7_15.0_19A5281h_Restore.ipsw]
| 5,210,812,794
|-
| 19A5281j
| [[Keys:SkySeed 19A5281j (iPod9,1)|iPod9,1]]
| {{date|2021|06|30}}
| [https://updates.cdn-apple.com/2021SummerSeed/fullrestores/071-61501/D533C22E-B7D0-4409-8F7F-C0B0ED81A1C8/iPodtouch_7_15.0_19A5281j_Restore.ipsw iPodtouch_7_15.0_19A5281j_Restore.ipsw]
| 5,217,247,234
|-
| 15.0 beta 3
| 19A5297e
| [[Keys:SkySeed 19A5297e (iPod9,1)|iPod9,1]]
| {{date|2021|07|14}}
| [https://updates.cdn-apple.com/2021SummerSeed/fullrestores/071-62997/DCBD1A15-E701-4D02-9253-A3AE92CFECEB/iPodtouch_7_15.0_19A5297e_Restore.ipsw iPodtouch_7_15.0_19A5297e_Restore.ipsw]
| 5,216,597,382
|-
| 15.0 beta 4
| 19A5307g
| [[Keys:SkySeed 19A5307g (iPod9,1)|iPod9,1]]
| {{date|2021|07|27}}
| [https://updates.cdn-apple.com/2021SummerSeed/fullrestores/071-75920/9E0B77CA-4CE2-4D63-B8D2-89A04C1CEAE1/iPodtouch_7_15.0_19A5307g_Restore.ipsw iPodtouch_7_15.0_19A5307g_Restore.ipsw]
| 5,187,780,121
|-
| 15.0 beta 5
| 19A5318f
| [[Keys:SkySeed 19A5318f (iPod9,1)|iPod9,1]]
| {{date|2021|08|10}}
| [https://updates.cdn-apple.com/2021SummerSeed/fullrestores/071-79991/B983038B-BADD-411D-A35B-41B72A9AA453/iPodtouch_7_15.0_19A5318f_Restore.ipsw iPodtouch_7_15.0_19A5318f_Restore.ipsw]
| 5,160,604,434
|-
| 15.0 beta 6
| 19A5325f
| [[Keys:SkySeed 19A5325f (iPod9,1)|iPod9,1]]
| {{date|2021|08|17}}
| [https://updates.cdn-apple.com/2021SummerSeed/fullrestores/071-84852/75EF0E12-7F17-4149-A4CF-F91BC7F0C6D7/iPodtouch_7_15.0_19A5325f_Restore.ipsw iPodtouch_7_15.0_19A5325f_Restore.ipsw]
| 5,132,992,096
|-
| 15.0 beta 7
| 19A5337a
| [[Keys:SkySeed 19A5337a (iPod9,1)|iPod9,1]]
| {{date|2021|08|25}}
| [https://updates.cdn-apple.com/2021SummerSeed/fullrestores/071-88929/98961B2D-2095-419A-BF89-DF1A7328069A/iPodtouch_7_15.0_19A5337a_Restore.ipsw iPodtouch_7_15.0_19A5337a_Restore.ipsw]
| 5,125,277,205
|-
| 15.0 beta 8
| 19A5340a
| [[Keys:SkySeed 19A5340a (iPod9,1)|iPod9,1]]
| {{date|2021|08|31}}
| [https://updates.cdn-apple.com/2021SummerSeed/fullrestores/071-89613/F4BE9D0A-B5A2-4C69-BE3D-55B33D617334/iPodtouch_7_15.0_19A5340a_Restore.ipsw iPodtouch_7_15.0_19A5340a_Restore.ipsw]
| 5,126,653,398
|-
| 15.0 [[Release Candidate|RC]]
| 19A344
| [[Keys:Sky 19A344 (iPod9,1)|iPod9,1]]
| {{date|2021|09|14}}
| [https://updates.cdn-apple.com/2021FallFCS/fullrestores/071-96697/A478C1C8-E8D6-4535-BE8E-E76E34BE43C2/iPodtouch_7_15.0_19A344_Restore.ipsw iPodtouch_7_15.0_19A344_Restore.ipsw]
| 5,161,941,325
|-
| 15.1 beta
| 19B5042h
| [[Keys:SkyBSeed 19B5042h (iPod9,1)|iPod9,1]]
| {{date|2021|09|21}}
| [https://updates.cdn-apple.com/2021FallSeed/fullrestores/002-04031/8D70CB1D-FAED-4B3A-B04E-B16A60C581A9/iPodtouch_7_15.1_19B5042h_Restore.ipsw iPodtouch_7_15.1_19B5042h_Restore.ipsw]
| 5,157,991,820
|-
| 15.1 beta 2
| 19B5052f
| [[Keys:SkyBSeed 19B5052f (iPod9,1)|iPod9,1]]
| {{date|2021|09|28}}
| [https://updates.cdn-apple.com/2021FallSeed/fullrestores/002-06623/E2FFDE38-C571-421F-9AEE-8BE2DE76B6EE/iPodtouch_7_15.1_19B5052f_Restore.ipsw iPodtouch_7_15.1_19B5052f_Restore.ipsw]
| 5,169,268,142
|-
| 15.1 beta 3
| 19B5060d
| [[Keys:SkyBSeed 19B5060d (iPod9,1)|iPod9,1]]
| {{date|2021|10|06}}
| [https://updates.cdn-apple.com/2021FallSeed/fullrestores/002-10307/9CC6579B-4DD9-450F-9F37-376037D46C4A/iPodtouch_7_15.1_19B5060d_Restore.ipsw iPodtouch_7_15.1_19B5060d_Restore.ipsw]
| 5,170,446,590
|-
| 15.1 beta 4
| 19B5068a
| [[Keys:SkyBSeed 19B5068a (iPod9,1)|iPod9,1]]
| {{date|2021|10|13}}
| [https://updates.cdn-apple.com/2021FallSeed/fullrestores/002-17513/47DEF771-BE3F-47D3-A070-048AB2132B42/iPodtouch_7_15.1_19B5068a_Restore.ipsw iPodtouch_7_15.1_19B5068a_Restore.ipsw]
| 5,172,739,442
|-
| 15.1 [[Release Candidate|RC]]
| 19B74
| [[Keys:SkyB 19B74 (iPod9,1)|iPod9,1]]
| {{date|2021|10|18}}
| [https://updates.cdn-apple.com/2021FallFCS/fullrestores/071-63842/304996CA-641F-4457-8B2C-06B841329BAB/iPodtouch_7_15.1_19B74_Restore.ipsw iPodtouch_7_15.1_19B74_Restore.ipsw]
| 5,198,467,204
|-
| 15.2 beta
| 19C5026i
| [[Keys:SkyCSeed 19C5026i (iPod9,1)|iPod9,1]]
| {{date|2021|10|27}}
| [https://updates.cdn-apple.com/2021FallSeed/fullrestores/002-19898/6AB5C6D9-799D-4FAB-AFF7-FB85B8F14519/iPodtouch_7_15.2_19C5026i_Restore.ipsw iPodtouch_7_15.2_19C5026i_Restore.ipsw]
| 5,211,837,863
|-
| 15.2 beta 2
| 19C5036e
| [[Keys:SkyCSeed 19C5036e (iPod9,1)|iPod9,1]]
| {{date|2021|11|09}}
| [https://updates.cdn-apple.com/2021FallSeed/fullrestores/002-28796/DECB82C7-02EA-411F-A420-4DFE1E4BA1AF/iPodtouch_7_15.2_19C5036e_Restore.ipsw iPodtouch_7_15.2_19C5036e_Restore.ipsw]
| 5,238,933,584
|-
| 15.2 beta 3
| 19C5044b
| [[Keys:SkyCSeed 19C5044b (iPod9,1)|iPod9,1]]
| {{date|2021|11|16}}
| [https://updates.cdn-apple.com/2021FallSeed/fullrestores/002-32046/35174939-C566-4D93-9C30-93EC48781492/iPodtouch_7_15.2_19C5044b_Restore.ipsw iPodtouch_7_15.2_19C5044b_Restore.ipsw]
| 5,211,570,647
|-
| 15.2 beta 4
| 19C5050b
| [[Keys:SkyCSeed 19C5050b (iPod9,1)|iPod9,1]]
| {{date|2021|12|02}}
| [https://updates.cdn-apple.com/2021FallSeed/fullrestores/002-40292/A648E862-A6FE-4231-BBD0-6456B84E6060/iPodtouch_7_15.2_19C5050b_Restore.ipsw iPodtouch_7_15.2_19C5050b_Restore.ipsw]
| 5,214,303,669
|-
| 15.2 [[Release Candidate|RC]]
| 19C56
| [[Keys:SkyC 19C56 (iPod9,1)|iPod9,1]]
| {{date|2021|12|07}}
| [https://updates.cdn-apple.com/2021FCSWinter/fullrestores/071-92084/410AAE00-15DD-425A-A153-4B8C79BD886B/iPodtouch_7_15.2_19C56_Restore.ipsw iPodtouch_7_15.2_19C56_Restore.ipsw]
| 5,197,482,134
|-
| 15.3 beta
| 19D5026g
| [[Keys:SkyDSeed 19D5026g (iPod9,1)|iPod9,1]]
| {{date|2021|12|17}}
| [https://updates.cdn-apple.com/2022WinterSeed/fullrestores/002-44477/4EDD81D7-2EB4-488E-8928-3C4628C413D7/iPodtouch_7_15.3_19D5026g_Restore.ipsw iPodtouch_7_15.3_19D5026g_Restore.ipsw]
| 5,208,337,170
|-
| 15.3 beta 2
| 19D5040e
| [[Keys:SkyDSeed 19D5040e (iPod9,1)|iPod9,1]]
| {{date|2022|01|12}}
| [https://updates.cdn-apple.com/2022WinterSeed/fullrestores/002-52608/5F7AEF6D-03C7-485B-A80A-BCCDBD2C7D48/iPodtouch_7_15.3_19D5040e_Restore.ipsw iPodtouch_7_15.3_19D5040e_Restore.ipsw]
| 5,199,730,108
|-
| 15.3 [[Release Candidate|RC]]
| 19D49
| [[Keys:SkyD 19D49 (iPod9,1)|iPod9,1]]
| {{date|2022|01|20}}
| [https://updates.cdn-apple.com/2022FCSWinter/fullrestores/002-55767/793E5D53-BFA7-46E9-AC5E-8005185C0D86/iPodtouch_7_15.3_19D49_Restore.ipsw iPodtouch_7_15.3_19D49_Restore.ipsw]
| 5,205,265,409
|-
| 15.4 beta
| 19E5209h
| [[Keys:SkyEchoSeed 19E5209h (iPod9,1)|iPod9,1]]
| {{date|2022|01|27}}
| [https://updates.cdn-apple.com/2022WinterSeed/fullrestores/002-59224/726A5A70-28B5-4775-961D-CAFC0AFF75F4/iPodtouch_7_15.4_19E5209h_Restore.ipsw iPodtouch_7_15.4_19E5209h_Restore.ipsw]
| 5,086,255,390
|-
| 15.4 beta 2
| 19E5219e
| [[Keys:SkyEchoSeed 19E5219e (iPod9,1)|iPod9,1]]
| {{date|2022|02|08}}
| [https://updates.cdn-apple.com/2022WinterSeed/fullrestores/002-60260/9F2658AC-54ED-4632-8FD9-83B4091122BB/iPodtouch_7_15.4_19E5219e_Restore.ipsw iPodtouch_7_15.4_19E5219e_Restore.ipsw]
| 5,073,356,318
|-
| 15.4 beta 3
| 19E5225g
| [[Keys:SkyEchoSeed 19E5225g (iPod9,1)|iPod9,1]]
| {{date|2022|02|15}}
| [https://updates.cdn-apple.com/2022WinterSeed/fullrestores/002-71434/D937E041-8F07-4C71-BBC5-4E2C4D0227E3/iPodtouch_7_15.4_19E5225g_Restore.ipsw iPodtouch_7_15.4_19E5225g_Restore.ipsw]
| 5,091,305,939
|-
| 15.4 beta 4
| 19E5235a
| [[Keys:SkyEchoSeed 19E5235a (iPod9,1)|iPod9,1]]
| {{date|2022|02|22}}
| [https://updates.cdn-apple.com/2022WinterSeed/fullrestores/002-74341/589D6CF5-6456-495F-8254-4639A32C679C/iPodtouch_7_15.4_19E5235a_Restore.ipsw iPodtouch_7_15.4_19E5235a_Restore.ipsw]
| 5,015,721,367
|-
| 15.4 beta 5
| 19E5241a
| [[Keys:SkyEchoSeed 19E5241a (iPod9,1)|iPod9,1]]
| {{date|2022|03|01}}
| [https://updates.cdn-apple.com/2022WinterSeed/fullrestores/002-74819/947D4444-1098-4545-A9CC-F4E075F915FC/iPodtouch_7_15.4_19E5241a_Restore.ipsw iPodtouch_7_15.4_19E5241a_Restore.ipsw]
| 5,007,975,864
|-
| 15.4 [[Release Candidate|RC]]
| 19E241
| [[Keys:SkyEcho 19E241 (iPod9,1)|iPod9,1]]
| {{date|2022|03|08}}
| [https://updates.cdn-apple.com/2022FCSWinter/fullrestores/071-09817/D664167E-2B1F-478C-A8C2-9FC2C31A7598/iPodtouch_7_15.4_19E241_Restore.ipsw iPodtouch_7_15.4_19E241_Restore.ipsw]
| 5,009,071,471
|-
| 15.5 beta
| 19F5047e
| [[Keys:SkyFSeed 19F5047e (iPod9,1)|iPod9,1]]
| {{date|2022|04|05}}
| [https://updates.cdn-apple.com/2022SpringSeed/fullrestores/002-85901/5431FD90-E692-461E-A04F-0EAC247FEE55/iPodtouch_7_15.5_19F5047e_Restore.ipsw iPodtouch_7_15.5_19F5047e_Restore.ipsw]
| 5,016,752,074
|-
| 15.5 beta 2
| 19F5057e
| [[Keys:SkyFSeed 19F5057e (iPod9,1)|iPod9,1]]
| {{date|2022|04|19}}
| [https://updates.cdn-apple.com/2022SpringSeed/fullrestores/002-87552/CFC8D3D6-4346-4079-9B2B-BB485FB25208/iPodtouch_7_15.5_19F5057e_Restore.ipsw iPodtouch_7_15.5_19F5057e_Restore.ipsw]
| 5,026,274,653
|-
| 15.5 beta 3
| 19F5062g
| [[Keys:SkyFSeed 19F5062g (iPod9,1)|iPod9,1]]
| {{date|2022|04|26}}
| [https://updates.cdn-apple.com/2022SpringSeed/fullrestores/002-89695/C456C259-AD28-4E39-989E-F69F96732A83/iPodtouch_7_15.5_19F5062g_Restore.ipsw iPodtouch_7_15.5_19F5062g_Restore.ipsw]
| 5,023,901,937
|-
| 15.5 beta 4
| 19F5070b
| [[Keys:SkyFSeed 19F5070b (iPod9,1)|iPod9,1]]
| {{date|2022|05|03}}
| [https://updates.cdn-apple.com/2022SpringSeed/fullrestores/002-94681/6C1ADDD5-953A-426B-B8D2-3E9E51BCB34B/iPodtouch_7_15.5_19F5070b_Restore.ipsw iPodtouch_7_15.5_19F5070b_Restore.ipsw]
| 5,015,808,206
|-
| 15.5 [[Release Candidate|RC]]
| 19F77
| [[Keys:SkyF 19F77 (iPod9,1)|iPod9,1]]
| {{date|2022|05|12}}
| [https://updates.cdn-apple.com/2022SpringFCS/fullrestores/012-07285/1493EB24-19E9-4B70-858B-D3BA9B530905/iPodtouch_7_15.5_19F77_Restore.ipsw iPodtouch_7_15.5_19F77_Restore.ipsw]
| 5,024,444,576
|-
| 15.6 beta
| 19G5027e
| [[Keys:SkyGSeed 19G5027e (iPod9,1)|iPod9,1]]
| {{date|2022|05|18}}
| [https://updates.cdn-apple.com/2022SpringSeed/fullrestores/002-94090/EF12157D-A52E-4F6B-A0BD-2762D8EDB835/iPodtouch_7_15.6_19G5027e_Restore.ipsw iPodtouch_7_15.6_19G5027e_Restore.ipsw]
| 5,018,626,716
|-
| 15.6 beta 2
| 19G5037d
| [[Keys:SkyGSeed 19G5037d (iPod9,1)|iPod9,1]]
| {{date|2022|05|31}}
| [https://updates.cdn-apple.com/2022SpringSeed/fullrestores/012-10582/641CB52B-3038-438A-B4D7-E5816B76DC0C/iPodtouch_7_15.6_19G5037d_Restore.ipsw iPodtouch_7_15.6_19G5037d_Restore.ipsw]
| 5,026,393,090
|-
| 15.6 beta 3
| 19G5046d
| [[Keys:SkyGSeed 19G5046d (iPod9,1)|iPod9,1]]
| {{date|2022|06|14}}
| [https://updates.cdn-apple.com/2022SpringSeed/fullrestores/012-18164/BDACB1C4-FE0E-4C24-83B0-3E9A3A121607/iPodtouch_7_15.6_19G5046d_Restore.ipsw iPodtouch_7_15.6_19G5046d_Restore.ipsw]
| 5,020,175,041
|-
| 15.6 beta 4
| 19G5056c
| [[Keys:SkyGSeed 19G5056c (iPod9,1)|iPod9,1]]
| {{date|2022|06|28}}
| [https://updates.cdn-apple.com/2022SpringSeed/fullrestores/012-32961/C45BDDD4-BDDD-4ADF-8384-37ABA00BDDCF/iPodtouch_7_15.6_19G5056c_Restore.ipsw iPodtouch_7_15.6_19G5056c_Restore.ipsw]
| 5,018,954,891
|-
| 15.6 beta 5
| 19G5063a
| [[Keys:SkyGSeed 19G5063a (iPod9,1)|iPod9,1]]
| {{date|2022|07|05}}
| [https://updates.cdn-apple.com/2022SpringSeed/fullrestores/012-36026/1F145ABF-FBAD-4054-90E6-C99A1B5A596F/iPodtouch_7_15.6_19G5063a_Restore.ipsw iPodtouch_7_15.6_19G5063a_Restore.ipsw]
| 5,018,722,252
|-
| 15.6 [[Release Candidate|RC]]
| 19G69
| [[Keys:SkyG 19G69 (iPod9,1)|iPod9,1]]
| {{date|2022|07|12}}
| [https://updates.cdn-apple.com/2022SummerFCS/fullrestores/012-40446/84AD01D6-DC79-424B-81C9-3EC733101645/iPodtouch_7_15.6_19G69_Restore.ipsw iPodtouch_7_15.6_19G69_Restore.ipsw]
| 5,020,978,721
|-
| 15.6 [[Release Candidate|RC]] 2
| 19G71
| [[Keys:SkyG 19G71 (iPod9,1)|iPod9,1]]
| {{date|2022|07|15}}
| [https://updates.cdn-apple.com/2022SummerFCS/fullrestores/012-41721/DB09B300-123D-4ED1-A2F8-7DEE648377A6/iPodtouch_7_15.6_19G71_Restore.ipsw iPodtouch_7_15.6_19G71_Restore.ipsw]
| 5,013,046,902
|-
| 15.7 [[Release Candidate|RC]]
| 19H12
| [[Keys:SkySecuritySydney 19H12 (iPod9,1)|iPod9,1]]
| {{date|2022|09|07}}
| [https://updates.cdn-apple.com/2022FallFCS/fullrestores/012-39095/C790F618-F409-4C41-B39B-F29B2168B4AC/iPodtouch_7_15.7_19H12_Restore.ipsw iPodtouch_7_15.7_19H12_Restore.ipsw]
| 5,020,913,297
|-
| 15.7.1 [[Release Candidate|RC]]
| 19H115
| [[Keys:SkySecuritySydneyB 19H115 (iPod9,1)|iPod9,1]]
| {{date|2022|10|18}}
| [https://updates.cdn-apple.com/2022FallFCS/fullrestores/012-91115/2D9781F4-4EDF-4491-9106-ED52E72DBABC/iPodtouch_7_15.7.1_19H115_Restore.ipsw iPodtouch_7_15.7.1_19H115_Restore.ipsw]
| 5,013,597,912
|-
| 15.7.2 [[Release Candidate|RC]]
| 19H218
| [[Keys:SkySecuritySydneyC 19H218 (iPod9,1)|iPod9,1]]
| {{date|2022|12|07}}
| [https://updates.cdn-apple.com/2022FallFCS/fullrestores/012-91667/AFEAC5CA-9EDB-4635-87EB-9AD1DE26D005/iPodtouch_7_15.7.2_19H218_Restore.ipsw iPodtouch_7_15.7.2_19H218_Restore.ipsw]
| 5,020,600,004
|-
| 15.7.3 [[Release Candidate|RC]]
| 19H307
| [[Keys:SkyUpdate 19H307 (iPod9,1)|iPod9,1]]
| {{date|2023|01|18}}
| [https://updates.cdn-apple.com/2023WinterFCS/fullrestores/032-18256/AC2BA9AF-F62E-45D0-8A4C-EEF6482D0694/iPodtouch_7_15.7.3_19H307_Restore.ipsw iPodtouch_7_15.7.3_19H307_Restore.ipsw]
| 5,013,263,603
|-
| 15.7.4 [[Release Candidate|RC]]
| 19H321
| [[Keys:SkyUpdate 19H321 (iPod9,1)|iPod9,1]]
| {{date|2023|03|21}}
| [https://updates.cdn-apple.com/2023WinterFCS/fullrestores/032-48089/C1E60A21-4597-44DC-AF25-171FA3EFF9FC/iPodtouch_7_15.7.4_19H321_Restore.ipsw iPodtouch_7_15.7.4_19H321_Restore.ipsw]
| 5,013,651,840
|-
| 15.7.6 [[Release Candidate|RC]]
| 19H349
| [[Keys:SkyUpdate 19H349 (iPod9,1)|iPod9,1]]
| {{date|2023|05|09}}
| [https://updates.cdn-apple.com/2023WinterFCS/fullrestores/032-73521/55D8376A-ECBD-4ED2-9872-CECE48A7D0E3/iPodtouch_7_15.7.6_19H349_Restore.ipsw iPodtouch_7_15.7.6_19H349_Restore.ipsw]
| 5,014,037,052
|-
| 15.7.8 [[Release Candidate|RC]]
| 19H364
| [[Keys:SkyUpdate 19H364 (iPod9,1)|iPod9,1]]
| {{date|2023|07|18}}
| [https://updates.cdn-apple.com/2023WinterFCS/fullrestores/042-11045/E2386537-EAE7-46BA-BEFE-50557BDC67DF/iPodtouch_7_15.7.8_19H364_Restore.ipsw iPodtouch_7_15.7.8_19H364_Restore.ipsw]
| 5,013,840,720
|-
| 15.8 [[Release Candidate|RC]]
| 19H370
| [[Keys:SkyUpdate 19H370 (iPod9,1)|iPod9,1]]
| {{date|2023|10|18}}
| [https://updates.cdn-apple.com/2023FallFCS/fullrestores/042-76365/3FAD7473-B248-49F4-9660-92AD1051EBEC/iPodtouch_7_15.8_19H370_Restore.ipsw iPodtouch_7_15.8_19H370_Restore.ipsw]
| 5,021,378,515
|-
| 15.8.1 [[Release Candidate|RC]]
| 19H380
| [[Keys:SkyUpdate 19H380 (iPod9,1)|iPod9,1]]
| {{date|2024|01|17}}
| [https://updates.cdn-apple.com/2024WinterFCS/fullrestores/052-23773/1915605D-E5E1-4628-96B8-4DF8C3F2B6C8/iPodtouch_7_15.8.1_19H380_Restore.ipsw iPodtouch_7_15.8.1_19H380_Restore.ipsw]
| 5,013,420,914
|-
| 15.8.2 [[Release Candidate|RC]]
| 19H384
| [[Keys:SkyUpdate 19H384 (iPod9,1)|iPod9,1]]
| {{date|2024|02|27}}
| [https://updates.cdn-apple.com/2024WinterFCS/fullrestores/052-42343/15344590-538C-4444-9F35-7126E2DD8565/iPodtouch_7_15.8.2_19H384_Restore.ipsw iPodtouch_7_15.8.2_19H384_Restore.ipsw]
| 5,013,640,869
|}

[[Category:Firmware]]