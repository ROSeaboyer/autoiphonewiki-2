# SPDX-FileCopyrightText: 2023 Nicolás Alvarez <nicolas.alvarez@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import time
import logging
import sys
import os
import re
import json

logging.basicConfig(format='%(asctime)-15s %(message)s')
log = logging.getLogger('uploader')
log.setLevel(logging.DEBUG)

def main():
    start_time = time.time()
    log.info("Load page")
    browser.get('https://theapplewiki.com/')
    with open("cookies.json", "r") as f:
        cookies = json.load(f)
        for name,value in cookies.items():
            browser.add_cookie({"name": name, "value": value})

    browser.get('https://theapplewiki.com/')

    for page_name in sys.argv[1:]:
        fn = page_name.replace("/", "_") + ".wiki"
        path = os.path.join("output", fn)
        new_page_content = open(path, "r").read()

        url = f'https://theapplewiki.com/wiki/{page_name}'
        browser.get(url)

        links = browser.find_elements_by_link_text("Edit")
        if len(links) != 1:
            log.warning("Didn't find edit page link! Page doesn't exist?")
            raise RuntimeError()

        links[0].click()

        log.info("Editing page")
        browser.find_element_by_id("wpTextbox1").clear()
        log.info("Running edit script")
        browser.execute_script('document.getElementById("wpTextbox1").value = ' + json.dumps(new_page_content)+";")
        log.info("done")
        browser.find_element_by_id("wpDiff").click()

        log.info("Waiting for submission...")
        WebDriverWait(browser, timeout=3600).until(lambda b: b.current_url == url)
        time.sleep(2)

log.info("Starting firefox")
start_time = time.time()
with webdriver.Firefox() as browser:
    log.info("We started Firefox in %.2f seconds" % (time.time()-start_time))
    main()
    log.info("Quitting")

