import json
import os
import plistlib
import base64
import subprocess

class MyEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, bytes):
            return base64.b64encode(o).decode('ascii')
        return json.JSONEncoder.default(self, o)

for f in os.listdir('result'):
    print(f)
    data = plistlib.load(open('result/'+f,'rb'))
    json.dump(data, open('manifest_cache/'+f.replace('plist','json'), 'w'), cls=MyEncoder)
    subprocess.check_call(["git", "add", "-f", 'manifest_cache/'+f.replace('plist','json')])
